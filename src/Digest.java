import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


// messageDigest = MessageDigest.getInstance("MD5");
//messageDigest.update(plainText);
//messageDigest.digest()


/**
 *
 * @author alex
 */
public class Digest {
    Boolean setPassword=false;

    public static void main(String[] args) {
        new Digest().init();
    }
    public  void init() {
        String contrassenya;
        String encoded = null;
        String spected = null;
        byte[] encodedBytes= null;
        byte[] spectedByes= null;
        byte[] plainText;
        
        
     //   plainText = new byte[]{'T', 'h', 'e', 'B', 'e', 's', 't', 'S', 'e', 'c', 'r', 'e', 't', 'K', 'e', 'y'};
        contrassenya = getPassword();
        plainText = contrassenya.getBytes();

//
// get a message digest object using the MD5 algorithm
        MessageDigest messageDigest = null;
        try {
            messageDigest = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Digest.class.getName()).log(Level.SEVERE, null, ex);
        }
//
// print out the provider used
        System.out.println("\n" + messageDigest.getProvider().getInfo());
//
// calculate the digest and print it out
        messageDigest.update(plainText);
                try {
            
            encodedBytes = messageDigest.digest();
            System.out.println(Arrays.toString(encodedBytes));
            encoded = new String(encodedBytes, "UTF8");
            
            if (setPassword){
                try {
                    System.out.println("Setting new password");

                    writeToFile("pass/password.bin", encodedBytes);
                } catch (IOException ex) {
                    Logger.getLogger(Digest.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
 /*           
 */
            
            System.out.println(encoded);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Digest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        try {
            spectedByes = getFileInBytes("pass/password.bin");
            spected = new String(spectedByes, "UTF8");
        } catch (IOException ex) {
            Logger.getLogger(Digest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
            System.out.println("Encoded : "+encoded);
            System.out.println("Espected: "+spected);
       
        if (Arrays.equals(spectedByes, encodedBytes)) {
            System.out.println("Correcte!");
        }
        else{
            System.out.println("Ho lamento, el password no és correcte!");
        }
    }
/*************************************************************************/
/*************************************************************************/
/*************************************************************************/
/*************************************************************************/
/*************************************************************************/
    public  String getPassword() {
        Scanner scan = new Scanner(System.in); //Creates a new scanner
        System.out.println("Enter password"); //Asks question
        String input = scan.nextLine(); //Waits for input
         System.out.println(">>>>>" + input);
        if (input.contains("set ")){
            
            setPassword = true;
            String parts[]=input.split(" ");
            input = parts[1];
            System.out.println(">>>>>"+input);
        }
        return input;
    }
    
    
	public void writeToFile(String path, byte[] key) throws IOException {
           
            System.out.println("Guardem el fitxer: " + path);
            
		File f = new File(path);
		f.getParentFile().mkdirs();

		FileOutputStream fos = new FileOutputStream(f);
                 
		fos.write(key);
		fos.flush();
		fos.close();
	}

        
	public byte[] getFileInBytes(String path) throws IOException {
                File f = new File(path);
		FileInputStream fis = new FileInputStream(f);
		byte[] fbytes = new byte[(int) f.length()];
		fis.read(fbytes);
		fis.close();
		return fbytes;
	}        
}
