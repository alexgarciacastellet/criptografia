/*
Fes un programa Java que demani una ruta d'un fitxer i una paraula de pas de 16 caracters (Ha de ser de 16 per generar una clau vàlida)
Amb aquests paràmetres hem d'encriptar / desencriptar el fitxer
Si el fitxer té extensió .txt entenem que l'hem d'encriptar generant un fitxer .aes
Si té extensio .aes entenem que el volem desencriptar generant un .txt

Heu de comprovar l'existència del fitxer i validar la longitud de la paraula de pas.
 */

import java.security.NoSuchAlgorithmException;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

import java.security.Key;
import java.util.Arrays;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author alex
 */
public class Cripto {

    private static final String ALGO = "AES";
    static String pass = "TheBestSecretKey";
    static String pass2 = "TheBestSecretKel";
    private static final byte[] keyValue = pass.getBytes();
    private static final byte[] keyValue2 = pass2.getBytes();
    private static String missatge = "AIXO ES UN MISSATGE";
    //     new byte[]{'T', 'h', 'e', 'B', 'e', 's', 't', 'S', 'e', 'c', 'r', 'e', 't', 'K', 'e', 'y'};
    // pass.getBytes();

    public static void main(String[] args) {
        Cripto crx = new Cripto();
        SecretKey sKey;
        String sortida = "";

        String encriptat = null;

        try {

            encriptat = crx.encrypt(missatge);
        } catch (Exception ex) {
            Logger.getLogger(Cripto.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            sortida = crx.decrypt(encriptat);
        } catch (Exception ex) {
            Logger.getLogger(Cripto.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.println(missatge + " >>>> " + encriptat + " >>>> " + sortida);

        try {
            byte[] encriptatBytes = crx.encryptBytes(missatge);
            encriptat = Arrays.toString(encriptatBytes);
            sortida = crx.decryptBytes(encriptatBytes);
        } catch (Exception ex) {
            Logger.getLogger(Cripto.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.println(missatge + " >>>> " + encriptat + " >>>> " + sortida);
    }

    /**
     * Encrypt a string with AES algorithm.
     *
     * @param data is a string
     * @return the encrypted string
     */
    public static String encrypt(String data) throws Exception {
        Key key = generateKey(keyValue2);
        Cipher c = Cipher.getInstance(ALGO);
        c.init(Cipher.ENCRYPT_MODE, key);
        byte[] encVal = c.doFinal(data.getBytes());
        return Base64.getEncoder().encodeToString(encVal);
    }

    public static byte[] encryptBytes(String data) throws Exception {
        Key key = generateKey(keyValue2);
        Cipher c = Cipher.getInstance(ALGO);
        c.init(Cipher.ENCRYPT_MODE, key);
        byte[] encVal = c.doFinal(data.getBytes());

        return encVal;
        //return Base64.getEncoder().encodeToString(encVal);
    }

    /**
     * Decrypt a string with AES algorithm.
     *
     * @param encryptedData is a string
     * @return the decrypted string
     */
    public static String decrypt(String encryptedData) throws Exception {
        Key key = generateKey(keyValue2);
        Cipher c = Cipher.getInstance(ALGO);
        c.init(Cipher.DECRYPT_MODE, key);
        byte[] decordedValue = Base64.getDecoder().decode(encryptedData);
        byte[] decValue = c.doFinal(decordedValue);

        return new String(decValue);
    }

    public static String decryptBytes(byte[] encryptedData) throws Exception {
        Key key = generateKey(keyValue2);
        Cipher c = Cipher.getInstance(ALGO);
        c.init(Cipher.DECRYPT_MODE, key);
        // byte[] decordedValue = Base64.getDecoder().decode(encryptedData);
        byte[] decValue = c.doFinal(encryptedData);

        return new String(decValue, "UTF8");
    }

    /**
     * Generate a new encryption key.
     */
    private static Key generateKey(byte[] keyValue) throws Exception {
        return new SecretKeySpec(keyValue, ALGO);

        // return keygenKeyGeneration(256);
    }
}
